.ONESHELL:
SHELL := /bin/bash
LISP ?= sbcl
PACKAGE_NAME = ddg-search
ASDFILE = ${PACKAGE_NAME}.asd
BINARYPATH = ~/.local/bin
QUICKLISP_PATH = ${HOME}/quicklisp/local-projects/
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))

build:
	@if [ -L ${QUICKLISP_PATH}${PACKAGE_NAME} ]; then
		ln -s ${current_dir} ${QUICKLISP_PATH}${PACKAGE_NAME}
	else
		echo "Already in quicklisp directory. No need to symlink."
	fi
	$(LISP) --load $(ASDFILE) \
		--eval "(ql:quickload :${PACKAGE_NAME})" \
		--eval "(asdf:make :${PACKAGE_NAME})" \
		--eval "(quit)"
install:
	mkdir -p ${BINARYPATH}
	cp -f ${PACKAGE_NAME} ${BINARYPATH}
	chmod 755 ${BINARYPATH}

clean:
	rm ${PACKAGE_NAME}
	rm *.fasl
