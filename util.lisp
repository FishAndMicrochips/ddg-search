(in-package #:ddg-search)

;;;; Parameters

(defparameter *terminal-args* nil)
(defparameter *familiar-sites* nil)

;;;; Macros

(defmacro define-terminal-arg (&key type var default-value documentation tags)
  (let ((var (or var (alexandria:symbolicate '* type '*))))
    `(progn
       (defparameter ,var ,default-value ,documentation)
       (push '(,type ,var ,tags) *terminal-args*))))

(defmacro multiple-macros (m &body things)
  `(progn
     ,@(mapcar (lambda (thing)
                 `(,m ,@thing))
               things)))

(defmacro define-familiar-site (&key tag command regex)
  `(push '(,tag ,command ,@(when regex (list regex)))
         *familiar-sites*))

(defmacro familiar-sites (&body forms)
  `(multiple-macros define-familiar-site ,@forms))

(defmacro terminal-arguments (&body args)
  `(multiple-macros define-terminal-arg ,@args))

(defmacro when-> (&body forms)
  (labels ((rec (forms &optional  last-result)
             (let ((expr `(,(first forms) ,@last-result)))
               (if (rest forms)
                   (alexandria:with-gensyms (result)
                     `(rutils:when-let (,result ,expr)
                        ,(rec (rest forms) (list result))))
                   expr))))
    (rec forms)))


;;;; Data Structures

(defstruct (familiar-site (:type list))
  datatype launcher regex)

(defstruct (result (:type list))
  datatype title url)

(defstruct (terminal-arg (:type list))
  tag var keywords)

;; Utility Functions

(defun find-in-tree (item tree &key (test #'eql) (key #'identity))
  "Find an element in an arbitrarily nested tree"
  (labels ((rec (tree)
             (cond ((funcall test item (funcall key tree))
                    tree)
                   ((consp tree)
                    (or (rec (first tree))
                        (rec (rest tree)))))))
    (rec tree)))


(defun verbose-return (prompt x)
  (when *verbose*
    (format t "~A: ~A~%" prompt x))
  x)

(defun parse-posix-args (args)
  (mapc (lambda (arg)
          (rutils:when-let (found (find arg *terminal-args*
                                        :key #'terminal-arg-keywords
                                        :test (lambda (thing list)
                                                (member thing list :test #'equal))))
            (destructuring-bind (tag var keywords) found
              (declare (ignore keywords))
              (cond ((equal tag 'help)
                     (help)
                     (uiop:quit))
                    ((equal tag 'switch)
                     (set var t))
                    ((equal tag 'parameter)
                     (set var (getf args arg)))
                    (t (error "Unknown terminal parameter type: ~A" found))))))
        args))

(defun help ()
  (mapc (lambda (arg) (apply #'help-arg arg))
        *terminal-args*)
  (format t "usage: ddg-search ~{~90<~a~@;~%                  ~>~}~%"
          (mapcar #'usage-string *terminal-args*)))

(defun help-arg (tag var keywords)
  (format t "~A ~A ~%Tags: ~A~%Documentation: ~A~%Default value: ~W~%~%"
          tag var keywords (documentation var 'variable) (eval var)))

(defun usage-string (terminal-arg)
  (format nil "~{[~{~A~^ ~}]~^ ~}"
          (let ((var (terminal-arg-var terminal-arg)))
            (mapcar (lambda (keyword)
                      `(,keyword ,@(when (equal 'parameter (terminal-arg-tag terminal-arg))
                                     (list var))))
                    (terminal-arg-keywords terminal-arg)))))

(defun my-command-line ()
  "The venerable function from Stack Overflow: https://stackoverflow.com/questions/1021778/getting-command-line-arguments-in-common-lisp#1021843"
  (or
   #+CLISP ext:*args*
   #+SBCL sb-ext:*posix-argv*
   nil))
