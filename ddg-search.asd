;;;; ddg-search.asd

(asdf:defsystem #:ddg-search
  :description "Describe ddg-search here"
  :author "Your Name <fishandmicrochips )at( disroot ] dot { org>"
  :license  "GPL v3.0"
  :version "0.0.1"
  :serial t
  :depends-on (:drakma :cl-html-parse :rutils :cl-ppcre :quri)
  :build-operation "program-op"
  :build-pathname "ddg-search"
  :entry-point "ddg-search:main"
  :components ((:file "package")
               (:file "util")
               (:file "config")
               (:file "dmenu")
               (:file "ddg-search")))
