(in-package #:ddg-search)

(familiar-sites
  (:tag html :command "surf ~a")
  (:tag pdf :command "firefox --new-window ~a")
  (:tag youtube :command "mpv ~a" :regex "https?.\/\/[a-z]*\.?youtube\.com\/watch"))

(terminal-arguments
  (:tags ("-h" "--help")
   :type help
   :documentation "Print out the help message")

  (:tags ("-v" "--verbose")
   :var *verbose*
   :type switch
   :documentation "Print out useful messages when searching. Handy for debugging.")

  (:tags ("--dmenu" "-d")
   :var *dmenu-template*
   :type parameter
   :default-value "dmenu -p \"DuckDuckGo Search\""
   :documentation "Change this if your dmenu command requires some specific settings")

  (:tags ("--results-list-header" "-rlh")
   :var *results-list-header*
   :type parameter
   :default-value "<div class=\"result results_links results_links_deep web-result \">"
   :documentation "This is the header that duckduckgo looks for when finding all the results.")

  (:tags ("--result-header" "-rh")
   :var *result-header*
   :type parameter
   :default-value "<h2 class=\"result__title\">"
   :documentation "This is the header we look for when extracting the relevant data from each result")

  (:tags ("--search-template" "-s")
   :var *search-template*
   :type parameter
   :default-value "https://duckduckgo.com/html/?q=~a"
   :documentation "Change this if you want to search through another search engine. Keep in mind that you will need to change the headers as well!"))
