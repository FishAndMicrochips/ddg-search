;;;; package.lisp

(defpackage #:ddg-search
  (:use #:cl)
  (:export #:main
           #:ddg-search
           #:*browser*
           #:*html-find*
           #:*search-template*
           #:*sanitize-substitutions*
	   #:*url-substitutions*
	   #:*name-substitutions*))
