(in-package #:ddg-search)

(defun pipe-to-dmenu (stream)
  (handler-case
      (with-output-to-string (out)
        (uiop:run-program
         *dmenu-template*
         :input stream
         :output out))
    (UIOP/RUN-PROGRAM:SUBPROCESS-ERROR () ;; Triggered by hitting ESC in Dmenu. ignore it.
      "")))

(defun list->dmenu (list)
  (pipe-to-dmenu (make-string-input-stream (format nil "~{~A~%~}" list))))
