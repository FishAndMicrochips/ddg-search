;;;; ddg-search.lisp

(in-package #:ddg-search)

(defun main ()
  (parse-posix-args (my-command-line))
  (when->
    get-query
    ddg-search
    select-result
    get-command
    uiop:launch-program))

(defun get-query ()
  (verbose-return
   "Query"
   (remove #\Newline (list->dmenu nil))))

(defun ddg-search (query)
  (when query
    (parse-results (extract-results (search-query query)))))

(defun select-result (results)
  (verbose-return
   "Selected result"
   (when results
     (find (list->dmenu (mapcar #'result-title results))
           results
           :test (lambda (x y)
                   (equal (remove #\Newline x)
                          (remove #\Newline y)))
           :key #'result-title))))

(defun get-command (result)
  (verbose-return
   "Launching"
   (format nil
           (familiar-site-launcher
            (find (result-datatype result) *familiar-sites*
                  :key #'familiar-site-datatype))
           (result-url result))))

(defun search-query (query &optional (template *search-template*))
  "Given a search query, Duckduckgo search the query."
  (verbose-return
   "Parsed HTML"
   (html-parse:parse-html
    (drakma:http-request
     (format nil template (quri:url-encode query))))))

(defun search-one-level-above (&key (test #'equal) (key #'identity))
  (lambda (needle haystack)
    (and (listp haystack)
         (member needle haystack :test test :key key))))

(defun html-find (html thing &key (test #'equal) (key #'identity))
  "Recursively scan down an S-expr analogue of HTML until an object is reached that contains the search term"
  (find-in-tree thing html
                ;; HTML is a little strange in that the thing you want to find is often
                ;; In a depth 1 deeper than what you want. E.g. ((:the :search :term) :stuff :I :need)
                :test (search-one-level-above :test test :key key)))

(defun extract-results (html)
  "Remove most of the junk from the results."
  (let ((results-list-header (parse-simple-html-lexeme *results-list-header*)))
    (remove-if-not (lambda (raw-html-result)
                     (and (listp raw-html-result)
                          (equal (first raw-html-result) results-list-header)))
                   (html-find html results-list-header :test (search-one-level-above)))))

(defun parse-simple-html-lexeme (header)
  (caar (html-parse:parse-html header)))

(defun parse-results (results)
  (mapcar #'interpret-result results))

(defun interpret-result (result)
  (verbose-return
   "Interpreted Result"
   (destructuring-bind (url name-or-type &optional name &rest other-junk)
       (second (html-find result (parse-simple-html-lexeme *result-header*)))
     (declare (ignore other-junk))
     (let ((url (remove-bollocks-from-url url)))
       (cond ((explicit-type name-or-type name) ;; The HTML makes the datatype explicit -- let's incorporate that
              (let ((datatype (read-from-string (second name-or-type))))
                (make-result :datatype datatype
                             :title (pretty-print-name name)
                             :url url)))
             ((stringp name-or-type) ;; We will have to implicitly find the type from a regex
              (make-result :datatype (recognize-this-site url)
                           :title (pretty-print-name name-or-type)
                           :url url))
             (t (list->dmenu "Error parsing website")))))))

(defun explicit-type (name-or-type name)
  (and (listp name-or-type) (stringp name)))

(defun recognize-this-site (url &optional (recognitions *familiar-sites*))
  (or (familiar-site-datatype
       (find url recognitions
             :key #'familiar-site-regex
             :test (lambda (url potential-site)
                     (and url potential-site
                          (cl-ppcre:scan potential-site url)))))
      'html))


(defun remove-bollocks-from-url (url-plist)
  (cl-ppcre:regex-replace-all ".*https?:\/\/"
                              (quri:url-decode (getf (rest url-plist) :href))
                              "https://"))

(defun pretty-print-name (name)
  (quri:url-decode (quri:url-encode name)))
